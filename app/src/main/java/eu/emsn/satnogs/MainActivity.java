package eu.emsn.satnogs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements MainChoiceAdapter.MainChoiceOnClickListener {

    Button vetOwnStation = null;
    Button vetOwnObservations = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        // Example to get settings
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean switchPref = sharedPref.getBoolean(SettingsActivity.KEY_PREF_EXAMPLE_SWITCH, false);

        RecyclerView list = findViewById(R.id.recycler_view_choices);

        MainChoice[] mainChoices = {
                new MainChoice("Vetting own observations", "Only select observation scheduled by you", R.drawable.sat_purple),
                new MainChoice("Vetting observations on own station", "Only select observations recorded by your station", R.drawable.ground_station_no_image)
        };

        MainChoiceAdapter adapter = new MainChoiceAdapter(mainChoices, this);
        list.setAdapter(adapter);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMainChoiceClick(int position) {
        Intent intent = new Intent(this, VettingObservationActivity.class);
        intent.putExtra("Type", position);
        startActivity(intent);
    }
}