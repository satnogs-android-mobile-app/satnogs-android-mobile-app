package eu.emsn.satnogs.dao;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "observations")
public class ObservationEntity {

    @PrimaryKey
    public int id;
    public int groundStationId;
    public int noradCatId;
    public String start;
    public String vettedStatus;
    public String waterFallURL;
    public String transmitterDescription;
    public long lastUpdate;
}
