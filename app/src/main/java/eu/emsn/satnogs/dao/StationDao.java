package eu.emsn.satnogs.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface StationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(StationEntity... station);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(StationEntity station);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateOne(StationEntity station);

    @Delete
    void delete(StationEntity... station);

    @Query("DELETE FROM stations")
    void deleteAll();

    @Query("SELECT * FROM stations")
    StationEntity[] getAll();

    @Query("SELECT * FROM stations where id = :id")
    StationEntity[] getStationById(int id);
}



