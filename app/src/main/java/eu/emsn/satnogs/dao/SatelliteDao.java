package eu.emsn.satnogs.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface SatelliteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(SatelliteEntity... satellite);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(SatelliteEntity satellite);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateOne(SatelliteEntity satellite);

    @Delete
    void delete(SatelliteEntity... satellite);

    @Query("DELETE FROM satellites")
    void deleteAll();

    @Query("SELECT * FROM satellites")
    SatelliteEntity[] getAll();

    @Query("SELECT * FROM satellites where norad_cat_id = :norad_cat_id")
    SatelliteEntity[] getSatelliteByNoradCatId(int norad_cat_id);

}
