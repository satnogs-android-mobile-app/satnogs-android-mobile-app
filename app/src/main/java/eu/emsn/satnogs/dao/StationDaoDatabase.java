package eu.emsn.satnogs.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {StationEntity.class}, version = 1, exportSchema = false)
public abstract class StationDaoDatabase extends RoomDatabase {

    private static StationDaoDatabase instance;

    public static synchronized StationDaoDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .inMemoryDatabaseBuilder(context, StationDaoDatabase.class)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract StationDao stationDao();
}
