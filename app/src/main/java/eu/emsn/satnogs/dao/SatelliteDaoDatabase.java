package eu.emsn.satnogs.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {SatelliteEntity.class}, version = 1, exportSchema = false)
public abstract class SatelliteDaoDatabase extends RoomDatabase {

    private static SatelliteDaoDatabase instance;

    public static synchronized SatelliteDaoDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .inMemoryDatabaseBuilder(context, SatelliteDaoDatabase.class)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract SatelliteDao satelliteDao();
}
