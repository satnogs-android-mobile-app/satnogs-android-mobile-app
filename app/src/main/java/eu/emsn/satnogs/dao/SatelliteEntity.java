package eu.emsn.satnogs.dao;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "satellites")
public class SatelliteEntity {

    @PrimaryKey
    public int norad_cat_id;
    public String imageURL;
    public String name;
    public String names;
    public long lastUpdate;
}
