package eu.emsn.satnogs.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {ObservationEntity.class}, version = 1, exportSchema = false)
public abstract class ObservationDaoDatabase extends RoomDatabase {

    private static ObservationDaoDatabase instance;

    public static synchronized ObservationDaoDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .inMemoryDatabaseBuilder(context, ObservationDaoDatabase.class)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract ObservationDao observationDao();
}
