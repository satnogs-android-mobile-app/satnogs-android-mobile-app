package eu.emsn.satnogs.dao;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "stations")
public class StationEntity {
    @PrimaryKey
    public int id;
    public String imageURL;
    public String name;
    public int observations;
    public long lastUpdate;
}
