package eu.emsn.satnogs.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ObservationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ObservationEntity... observation);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(ObservationEntity observation);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateOne(ObservationEntity observation);

    @Delete
    void delete(ObservationEntity... observation);

    @Query("DELETE FROM observations")
    void deleteAll();

    @Query("SELECT * FROM observations")
    ObservationEntity[] getAll();

    @Query("SELECT * FROM observations where id = :id")
    ObservationEntity[] getObservationByNoradCatId(int id);

    @Query("SELECT * from observations where vettedStatus = 'unknown'")
    ObservationEntity[] getAllUnvettedObservations();

    @Query("UPDATE observations SET vettedStatus = :status WHERE id = :id")
    void vetObservation(int id, String status);
}
