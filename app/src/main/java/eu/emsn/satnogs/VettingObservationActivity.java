package eu.emsn.satnogs;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import eu.emsn.satnogs.dao.ObservationEntity;
import eu.emsn.satnogs.dao.SatelliteEntity;
import eu.emsn.satnogs.dao.StationEntity;

public class VettingObservationActivity extends AppCompatActivity implements View.OnClickListener {

    private ObservationWrapper observationWrapper = null;
    private StationWrapper stationWrapper = null;
    private SatelliteWrapper satelliteWrapper = null;

    private ImageView imageViewWaterfall = null;
    private ImageView imageViewSatellite = null;
    private ImageView imageViewStation = null;

    private TextView textViewSatName = null;
    private TextView textViewObservationTime = null;
    private TextView textViewFrequency = null;
    private TextView textViewMode = null;
    private TextView textViewGroundStationName = null;
    private TextView textViewOwner = null;
    private TextView textViewNumberObservations = null;

    private ImageButton imageButtonRefresh = null;
    private ImageButton imageButtonWithSignal = null;
    private ImageButton imageButtonWithoutSignal = null;

    private ObservationEntity observation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vetting_observation);
        int type = getIntent().getExtras().getInt("Type");

        observationWrapper = new ObservationWrapper(getApplicationContext());
        stationWrapper = new StationWrapper(getApplicationContext());
        satelliteWrapper = new SatelliteWrapper(getApplicationContext());

        imageViewWaterfall = findViewById(R.id.imageViewVettingWaterfall);
        imageViewSatellite = findViewById(R.id.imageViewVettingSat);
        imageViewStation = findViewById(R.id.imageViewVettingGroundStation);

        textViewSatName = findViewById(R.id.textViewVettingSatName);
        textViewObservationTime = findViewById(R.id.textViewVettingObservationTime);
        textViewFrequency = findViewById(R.id.textViewVettingFrequency);
        textViewMode = findViewById(R.id.textViewVettingSatMode);
        textViewGroundStationName = findViewById(R.id.textViewVettingGroundStationName);
        textViewOwner = findViewById(R.id.textViewVettingGroundStationOwner);
        textViewNumberObservations = findViewById(R.id.textViewVettingGroundStationNumberRecordings);

        imageButtonRefresh = findViewById(R.id.imageButtonVettingRefresh);
        imageButtonRefresh.setOnClickListener(this);
        imageButtonWithSignal = findViewById(R.id.imageButtonVettingWithSignal);
        imageButtonWithSignal.setOnClickListener(this);
        imageButtonWithoutSignal = findViewById(R.id.imageButtonVettingWithOutSignal);
        imageButtonWithoutSignal.setOnClickListener(this);

//        if ("OwnStation".equals(type)) {
        observationWrapper.getObservationForStationFromBackend(1492);
//        } else if ("OwnObservations".equals(type)) {
//            observationWrapper.getObservationForStationFromBackend(1492);
//        } else {
//            observationWrapper.getObservationForStationFromBackend(1492);
//        }

    }

    @Override
    public void onClick(View v) {

        if (v == imageButtonRefresh) {
            observation = observationWrapper.getUnvettedObservation();
        }

        if (v == imageButtonWithSignal) {
            if (observation != null) {
                observationWrapper.vetObservation(observation.id, "good");
            }
            observation = observationWrapper.getUnvettedObservation();
        }

        if (v == imageButtonWithoutSignal) {
            if (observation != null) {
                observationWrapper.vetObservation(observation.id, "bad");
            }
            observation = observationWrapper.getUnvettedObservation();
        }

        if (observation != null) {
            StationEntity station = stationWrapper.getStation(observation.groundStationId);
            if (station != null) {
                Picasso.get().load(station.imageURL).into(imageViewStation);
                textViewGroundStationName.setText(station.name);
                textViewOwner.setText("Diddi Stulle");
                textViewNumberObservations.setText("" + station.observations + " Observations");
            } else {
                Picasso.get().load(R.drawable.ground_station_no_image).into(imageViewStation);
                textViewGroundStationName.setText("Loading");
                textViewOwner.setText("Loading");
                textViewNumberObservations.setText("Loading");
            }

            SatelliteEntity sat = satelliteWrapper.getSatellite(observation.noradCatId);
            if (sat != null) {
                Picasso.get().load(sat.imageURL).into(imageViewSatellite);
                String satName = sat.name;
                if (sat.names != null && sat.names.length() > 0) {
                    satName += " (" + sat.names + ")";
                }
                textViewSatName.setText(satName);
            } else {
                Picasso.get().load(R.drawable.sat_purple).into(imageViewSatellite);
                textViewSatName.setText("Loading");
            }

            Picasso.get().load(observation.waterFallURL).into(imageViewWaterfall);
            textViewObservationTime.setText(observation.start.replace("T", " ").replace("Z", " "));
            textViewMode.setText(observation.transmitterDescription);
        }

    }
}