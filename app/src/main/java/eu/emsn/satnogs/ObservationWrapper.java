package eu.emsn.satnogs;

import android.content.Context;

import eu.emsn.satnogs.api.NetworkAPIClient;
import eu.emsn.satnogs.api.response.ObservationResponse;
import eu.emsn.satnogs.dao.ObservationDaoDatabase;
import eu.emsn.satnogs.dao.ObservationEntity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObservationWrapper {
    ObservationDaoDatabase db = null;

    public ObservationWrapper(Context context) {
        db = ObservationDaoDatabase.getInstance(context);
    }

    public void getObservationForStationFromBackend(int station) {
        Call<ObservationResponse> call = NetworkAPIClient
                .getInstance()
                .getApi()
                .getObservations(0, "unknown", station, 1);

        call.enqueue(new Callback<ObservationResponse>() {
            @Override
            public void onResponse(Call<ObservationResponse> call, Response<ObservationResponse> response) {
                if (response.code() == 200) {
                    ObservationResponse observationResponse = response.body();
                    if (observationResponse != null && observationResponse.size() > 0) {
                        for (int i = 0; i < observationResponse.size(); i++) {
                            if (observationResponse.get(i).getWaterfall() != null && observationResponse.get(i).getWaterfall() != "") {
                                ObservationEntity oe = new ObservationEntity();
                                oe.id = observationResponse.get(i).getId();
                                oe.groundStationId = observationResponse.get(i).getGround_station();
                                oe.noradCatId = observationResponse.get(i).getNorad_cat_id();
                                oe.start = observationResponse.get(i).getStart();
                                oe.vettedStatus = observationResponse.get(i).getVetted_status();
                                oe.waterFallURL = observationResponse.get(i).getWaterfall();
                                oe.transmitterDescription = observationResponse.get(i).getTransmitter_description();
                                oe.lastUpdate = System.currentTimeMillis();
                                db.observationDao().insertOne(oe);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ObservationResponse> call, Throwable t) {

            }
        });
    }

    public ObservationEntity getUnvettedObservation() {
        ObservationEntity[] entries = db.observationDao().getAllUnvettedObservations();

        if (entries != null && entries.length > 0) {
            return entries[0];
        } else {
//            getObservationForStationFromBackend(1492);
        }

        return null;
    }

    public void vetObservation(int id, String status) {
        db.observationDao().vetObservation(id, status);
    }
}

