package eu.emsn.satnogs;

import android.content.Context;

import eu.emsn.satnogs.api.NetworkAPIClient;
import eu.emsn.satnogs.api.response.StationResponse;
import eu.emsn.satnogs.dao.StationDaoDatabase;
import eu.emsn.satnogs.dao.StationEntity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StationWrapper {
    StationDaoDatabase db = null;

    public StationWrapper(Context context) {
        db = StationDaoDatabase.getInstance(context);
    }

    private void getStationFromBackend(int id) {
        Call<StationResponse> call = NetworkAPIClient
                .getInstance()
                .getApi()
                .getStation(id);

        call.enqueue(new Callback<StationResponse>() {
            @Override
            public void onResponse(Call<StationResponse> call, Response<StationResponse> response) {
                if (response.code() == 200) {
                    StationResponse stationResponse = response.body();
                    if (stationResponse != null && stationResponse.size() > 0) {
                        StationEntity se = new StationEntity();
                        se.id = stationResponse.get(0).getId();
                        se.name = stationResponse.get(0).getName();
                        se.imageURL = "https://network-satnogs.freetls.fastly.net/media/ground_stations/37f6f1ac-1262-4a4e-8812-ca7d80bf4663.jpeg"; //TODO: Wait for ticket
                        se.observations = stationResponse.get(0).getObservations();
                        se.lastUpdate = System.currentTimeMillis();
                        db.stationDao().insertOne(se);
                    }
                }
            }

            @Override
            public void onFailure(Call<StationResponse> call, Throwable t) {

            }
        });
    }

    public StationEntity getStation(int id) {
        StationEntity[] entries = db.stationDao().getStationById(id);

        if (entries.length > 0) {
            return entries[0];
        } else {
            getStationFromBackend(id);
        }

        return null;
    }

}
