package eu.emsn.satnogs;

import android.content.Context;

import eu.emsn.satnogs.api.DBAPIClient;
import eu.emsn.satnogs.api.response.SatelliteResponse;
import eu.emsn.satnogs.dao.SatelliteDaoDatabase;
import eu.emsn.satnogs.dao.SatelliteEntity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SatelliteWrapper {

    SatelliteDaoDatabase db = null;

    public SatelliteWrapper(Context context) {
        db = SatelliteDaoDatabase.getInstance(context);
    }

    private void getSatelliteFromBackend(int norad_cat_id) {
        Call<SatelliteResponse> call = DBAPIClient
                .getInstance()
                .getApi()
                .getSatellite(norad_cat_id);

        call.enqueue(new Callback<SatelliteResponse>() {
            @Override
            public void onResponse(Call<SatelliteResponse> call, Response<SatelliteResponse> response) {
                if (response.code() == 200) {
                    SatelliteResponse satelliteResponse = response.body();
                    if (satelliteResponse != null && satelliteResponse.size() > 0) {
                        SatelliteEntity se = new SatelliteEntity();
                        se.norad_cat_id = satelliteResponse.get(0).getNorad_cat_id();
                        se.imageURL = satelliteResponse.get(0).getImage();
                        se.name = satelliteResponse.get(0).getName();
                        se.names = satelliteResponse.get(0).getNames();
                        se.lastUpdate = System.currentTimeMillis();
                        db.satelliteDao().insertOne(se);
                    }
                }
            }

            @Override
            public void onFailure(Call<SatelliteResponse> call, Throwable t) {

            }
        });
    }

    public SatelliteEntity getSatellite(int norad_cat_id) {
        SatelliteEntity[] entries = db.satelliteDao().getSatelliteByNoradCatId(norad_cat_id);

        if (entries.length > 0) {
            return entries[0];
        } else {
            getSatelliteFromBackend(norad_cat_id);
        }

        return null;
    }
}
