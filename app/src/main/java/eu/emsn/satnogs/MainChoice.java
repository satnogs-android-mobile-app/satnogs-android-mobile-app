package eu.emsn.satnogs;

public class MainChoice {

    String name;
    String description;
    int image;

    MainChoice(String name, String description, int image) {
        this.name = name;
        this.description = description;
        this.image = image;
    }

}
