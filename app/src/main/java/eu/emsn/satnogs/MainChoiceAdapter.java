package eu.emsn.satnogs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MainChoiceAdapter extends RecyclerView.Adapter<MainChoiceAdapter.MainChoiceViewHolder> {

    private final MainChoice[] mainChoices;
    private final MainChoiceOnClickListener mainChoiceOnClickListener;

    public MainChoiceAdapter(MainChoice[] mainChoices, MainChoiceOnClickListener mainChoiceOnClickListener) {
        this.mainChoices = mainChoices;
        this.mainChoiceOnClickListener = mainChoiceOnClickListener;
    }

    @Override
    public int getItemCount() {
        return mainChoices.length;
    }

    @NonNull
    @Override
    public MainChoiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_choice, parent, false);
        return new MainChoiceViewHolder(view, mainChoiceOnClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MainChoiceViewHolder holder, int position) {
        holder.bind(mainChoices[position]);
    }

    public interface MainChoiceOnClickListener {
        void onMainChoiceClick(int position);
    }

    static class MainChoiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView appImage;
        TextView appTitle;
        TextView appDescription;
        MainChoiceOnClickListener mainChoiceOnClickListener;

        public MainChoiceViewHolder(@NonNull View itemView, MainChoiceOnClickListener mainChoiceOnClickListener) {
            super(itemView);
            appImage = itemView.findViewById(R.id.image_view_main_choice_icon);
            appTitle = itemView.findViewById(R.id.text_view_main_choice_title);
            appDescription = itemView.findViewById(R.id.text_view_main_choice_description);
            this.mainChoiceOnClickListener = mainChoiceOnClickListener;
            itemView.setOnClickListener(this);
        }

        public void bind(MainChoice mainChoice) {
            appTitle.setText(mainChoice.name);
            appDescription.setText(mainChoice.description);
            appImage.setImageResource(mainChoice.image);
        }

        @Override
        public void onClick(View v) {
            mainChoiceOnClickListener.onMainChoiceClick(getAbsoluteAdapterPosition());
        }
    }
}
