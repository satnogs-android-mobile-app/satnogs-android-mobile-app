package eu.emsn.satnogs.api.response

data class StationResponseItem(
    val altitude: Int,
    val antenna: List<Antenna>,
    val client_version: String,
    val created: String,
    val description: String,
    val id: Int,
    val last_seen: String,
    val lat: Double,
    val lng: Double,
    val min_horizon: Int,
    val name: String,
    val observations: Int,
    val qthlocator: String,
    val status: String,
    val target_utilization: Int
)