package eu.emsn.satnogs.api.response

data class Antenna(
    val antenna_type: String,
    val antenna_type_name: String,
    val band: String,
    val frequency: Int,
    val frequency_max: Int
)