package eu.emsn.satnogs.api.response

data class SatelliteResponseItem(
    val countries: String,
    val decayed: String,
    val deployed: String,
    val image: String,
    val launched: String,
    val name: String,
    val names: String,
    val norad_cat_id: Int,
    val `operator`: String,
    val status: String,
    val telemetries: List<Telemetry>,
    val website: String
)
