package eu.emsn.satnogs.api;

import eu.emsn.satnogs.api.response.SatelliteResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DBApi {

    @GET("/api/satellites/")
    Call<SatelliteResponse> getSatellite(
            @Query("norad_cat_id") int norad_cat_id
    );

}




