package eu.emsn.satnogs.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DBAPIClient {

    private static final String BASE_URL = "https://db.satnogs.org";

    private static DBAPIClient mInstance;
    private final Retrofit retrofit;

    private DBAPIClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized DBAPIClient getInstance() {
        if (mInstance == null) {
            mInstance = new DBAPIClient();
        }
        return mInstance;
    }

    public DBApi getApi() {
        return retrofit.create(DBApi.class);
    }
}
