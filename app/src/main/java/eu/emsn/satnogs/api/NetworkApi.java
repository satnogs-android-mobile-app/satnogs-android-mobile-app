package eu.emsn.satnogs.api;

import eu.emsn.satnogs.api.response.ObservationResponse;
import eu.emsn.satnogs.api.response.StationResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkApi {
    //    @FormUrlEncoded
    @GET("/api/observations/")
    Call<ObservationResponse> getObservations(
            @Query("author") int author,
            @Query("status") String status,
            @Query("ground_station") int ground_station,
            @Query("page") int page
    );

    @GET("/api/stations/")
    Call<StationResponse> getStation(
            @Query("id") int id
    );

}


