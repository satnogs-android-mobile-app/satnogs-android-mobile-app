package eu.emsn.satnogs.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkAPIClient {

    private static final String BASE_URL = "https://network.satnogs.org";

    private static NetworkAPIClient mInstance;
    private final Retrofit retrofit;

    private NetworkAPIClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized NetworkAPIClient getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkAPIClient();
        }
        return mInstance;
    }

    public NetworkApi getApi() {
        return retrofit.create(NetworkApi.class);
    }
}
